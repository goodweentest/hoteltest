package by.fedorov.hotel;

import by.fedorov.hotel.configuration.SecurityConstants;
import by.fedorov.hotel.controller.ApiMapping;
import by.fedorov.hotel.domain.HotelBooking;
import by.fedorov.hotel.domain.HotelCleaningReservation;
import by.fedorov.hotel.domain.HotelRoom;
import by.fedorov.hotel.domain.HotelWorkUnit;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class HotelApplicationTests {

    @LocalServerPort
    private int port;

    private MockMvc mvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ObjectMapper objectMapper;

    private String localhost;

    @Before
    public void setup() {
        this.localhost = "http://localhost:" + port;
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }

    @Test
    @WithMockUser(roles = {SecurityConstants.ADMIN})
    public void testACreateFirstRoomIsOk() throws Exception {
        HotelRoom hotelRoom = new HotelRoom();
        hotelRoom.setPlaces(2);
        hotelRoom.setPrice(40_000L);
        hotelRoom.setTitle("Room 1");

        mvc.perform(
                post(localhost + ApiMapping.ROOMS)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(hotelRoom))
                        )

                .andExpect(status().isOk())
                .andExpect(content().json("{id: 1, places: 2, price: 40000, title: 'Room 1'}"));
    }

    @Test
    @WithMockUser(roles = {SecurityConstants.ADMIN})
    public void testBCreateSecondRoomIsOk() throws Exception {
        HotelRoom hotelRoom = new HotelRoom();
        hotelRoom.setPlaces(1);
        hotelRoom.setPrice(10_000L);
        hotelRoom.setTitle("Room 2");

        mvc.perform(
                post(localhost + ApiMapping.ROOMS)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(hotelRoom)))
                .andExpect(status().isOk())
                .andExpect(content().json("{id: 2, places: 1, price: 10000, title: 'Room 2'}"));
    }

    private ResultActions createHotelWorkUnit(HotelWorkUnit hotelWorkUnit) throws Exception {
        return mvc.perform(
                post(localhost + ApiMapping.WORK_UNITS)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(hotelWorkUnit))
        );
    }

    @Test
    @WithMockUser(roles = {SecurityConstants.ADMIN})
    public void testCCreateHotelWorkUnitsIsOk() throws Exception {
        createHotelWorkUnit(
                new HotelWorkUnit(DayOfWeek.MONDAY, LocalTime.of(9, 0), LocalTime.of(21, 0))
        ).andExpect(status().isOk());
        createHotelWorkUnit(
                new HotelWorkUnit(DayOfWeek.TUESDAY, LocalTime.of(9, 0), LocalTime.of(21, 0))
        ).andExpect(status().isOk());
        createHotelWorkUnit(
                new HotelWorkUnit(DayOfWeek.WEDNESDAY, LocalTime.of(9, 0), LocalTime.of(21, 0))
        ).andExpect(status().isOk());
        createHotelWorkUnit(
                new HotelWorkUnit(DayOfWeek.THURSDAY, LocalTime.of(9, 0), LocalTime.of(21, 0))
        ).andExpect(status().isOk());
        createHotelWorkUnit(
                new HotelWorkUnit(DayOfWeek.FRIDAY, LocalTime.of(11, 0), LocalTime.of(18, 0))
        ).andExpect(status().isOk());
    }

    @Test
    public void testDGetHotelWorkUnitsIsOk() throws Exception {
        mvc.perform(
                get(localhost + ApiMapping.WORK_UNITS))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(5)));
    }

    private ResultActions createCleaningReservation(int roomId, HotelCleaningReservation reservation) throws Exception {
        return mvc.perform(
                post(localhost + ApiMapping.ROOMS + "/" + roomId + "/cleaningReservations")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(reservation))
        );
    }

    @Test
    @WithMockUser(roles = {SecurityConstants.ADMIN})
    public void testECreateCleaningReservation() throws Exception {
        HotelCleaningReservation cleaningReservation = new HotelCleaningReservation();
        cleaningReservation.setStartReservation(LocalDateTime.of(2019, 7, 1, 12, 0));
        cleaningReservation.setEndReservation(LocalDateTime.of(2019, 7, 1, 13, 0));

        createCleaningReservation(1, cleaningReservation)
                .andExpect(status().isOk());

        cleaningReservation = new HotelCleaningReservation();
        cleaningReservation.setStartReservation(LocalDateTime.of(2019, 7, 5, 11, 0));
        cleaningReservation.setEndReservation(LocalDateTime.of(2019, 7, 5, 18, 0));

        createCleaningReservation(1, cleaningReservation)
                .andExpect(status().isOk());

        cleaningReservation = new HotelCleaningReservation();
        cleaningReservation.setStartReservation(LocalDateTime.of(2019, 7, 2, 12, 0));
        cleaningReservation.setEndReservation(LocalDateTime.of(2019, 7, 2, 13, 0));

        createCleaningReservation(1, cleaningReservation)
                .andExpect(status().isOk());

        cleaningReservation = new HotelCleaningReservation();
        cleaningReservation.setStartReservation(LocalDateTime.of(2019, 7, 1, 11, 0));
        cleaningReservation.setEndReservation(LocalDateTime.of(2019, 7, 1, 12, 0));

        createCleaningReservation(2, cleaningReservation)
                .andExpect(status().isOk());

        cleaningReservation = new HotelCleaningReservation();
        cleaningReservation.setStartReservation(LocalDateTime.of(2019, 7, 2, 11, 0));
        cleaningReservation.setEndReservation(LocalDateTime.of(2019, 7, 2, 12, 0));

        createCleaningReservation(2, cleaningReservation)
                .andExpect(status().isOk());

        cleaningReservation = new HotelCleaningReservation();
        cleaningReservation.setStartReservation(LocalDateTime.of(2019, 7, 5, 13, 0));
        cleaningReservation.setEndReservation(LocalDateTime.of(2019, 7, 5, 14, 0));

        createCleaningReservation(2, cleaningReservation)
                .andExpect(status().isOk());
    }

    @Test
    public void testFGetFreeRomsAndExpectOneRoom() throws Exception {
        mvc.perform(get(localhost + ApiMapping.ROOMS)
                .contentType(MediaType.APPLICATION_JSON)
                .param("start", "2019-07-01 09:00")
                .param("end", "2019-07-01 12:00"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)));
    }

    @Test
    public void testGGetFreeRomsAndExpectOneRoom() throws Exception {
        mvc.perform(get(localhost + ApiMapping.ROOMS)
                .contentType(MediaType.APPLICATION_JSON)
                .param("start", "2019-07-05 14:00")
                .param("end", "2019-07-05 18:00"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)));
    }

    @Test
    @WithMockUser(roles = {SecurityConstants.USER})
    public void testHCreateFirstBookingIsOk() throws Exception {
        HotelBooking hotelBooking = new HotelBooking();
        hotelBooking.setStartReservation(LocalDateTime.of(2019, 7, 1, 10, 40));
        hotelBooking.setEndReservation(LocalDateTime.of(2019, 7, 1, 12, 0));
        mvc.perform(post(localhost + ApiMapping.ROOMS + "/1/userBookings")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(hotelBooking)))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(roles = {SecurityConstants.USER})
    public void testICreateSecondBookingConflict() throws Exception {
        HotelBooking hotelBooking = new HotelBooking();
        hotelBooking.setStartReservation(LocalDateTime.of(2019, 7, 1, 8, 30));
        hotelBooking.setEndReservation(LocalDateTime.of(2019, 7, 1, 12, 0));
        mvc.perform(post(localhost + ApiMapping.ROOMS + "/1/userBookings")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(hotelBooking)))
                .andExpect(status().is(409));
    }

    @Test
    @WithMockUser(roles = {SecurityConstants.USER})
    public void testJCreateThirdBookingConflict() throws Exception {
        HotelBooking hotelBooking = new HotelBooking();
        hotelBooking.setStartReservation(LocalDateTime.of(2019, 7, 2, 12, 30));
        hotelBooking.setEndReservation(LocalDateTime.of(2019, 7, 2, 14, 0));
        mvc.perform(post(localhost + ApiMapping.ROOMS + "/1/userBookings")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(hotelBooking)))
                .andExpect(status().is(409));
    }

}
