package by.fedorov.hotel.business;

import by.fedorov.hotel.domain.HotelReservation;
import by.fedorov.hotel.domain.HotelRoom;
import by.fedorov.hotel.domain.HotelWorkUnit;
import by.fedorov.hotel.repository.HotelReservationTimeRepository;
import by.fedorov.hotel.repository.HotelRoomRepository;
import by.fedorov.hotel.repository.HotelWorkUnitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Service
public class HotelService implements ReservationService, WorkUnitService, RoomService {

    @Autowired
    private HotelWorkUnitRepository hotelWorkUnitRepository;

    @Autowired
    private HotelRoomRepository hotelRoomRepository;

    @Autowired
    private HotelReservationTimeRepository hotelReservationTimeRepository;

    private Lock reservationLock = new ReentrantLock();

    @Override
    public HotelWorkUnit saveWorkUnit(HotelWorkUnit hotelWorkUnit) {
        return hotelWorkUnitRepository.save(hotelWorkUnit);
    }

    @Override
    public List<HotelWorkUnit> listWorkUnits() {
        return hotelWorkUnitRepository.findAll();
    }

    @Override
    public HotelRoom createHotelRoom(HotelRoom hotelRoom) {
        if (hotelRoom.getId() != null) throw new HotelApiException("create room exception");
        return hotelRoomRepository.save(hotelRoom);
    }

    @Override
    public HotelRoom updateHotelRoom(HotelRoom hotelRoom) {
        return hotelRoomRepository.save(hotelRoom);
    }

    @Override
    public List<HotelRoom> listFreeRooms(LocalDateTime start, LocalDateTime end) {
        return hotelRoomRepository.findFreeRooms(start, end);
    }

    @Override
    public <T extends HotelReservation> T reserve(Integer roomId, T hotelReservationTime) {
        LocalDateTime start = hotelReservationTime.getStartReservation();
        LocalDateTime end = hotelReservationTime.getEndReservation();
        if (end.isBefore(start)) {
            throw new HotelApiException("time duration exception");
        }
        if (!start.toLocalDate().equals(end.toLocalDate())) {
            throw new HotelApiException("reservation only time");
        }
        DayOfWeek dayOfWeek = start.getDayOfWeek();
        Optional<HotelWorkUnit> hotelWorkUnit = hotelWorkUnitRepository.findById(dayOfWeek);
        if (hotelWorkUnit.isEmpty()) {
            throw new HotelApiException("hotel doesn't work (work unit exception)");
        }
        if (hotelWorkUnit.get().getStartTime().isAfter(start.toLocalTime()) ||
                hotelWorkUnit.get().getEndTime().isBefore(end.toLocalTime())) {
            throw new HotelApiException("hotel doesn't work (reservation time exception)");
        }
        Optional<HotelRoom> hotelRoom = hotelRoomRepository.findById(roomId);
        if (hotelRoom.isEmpty()) {
            throw new HotelApiException("hotel room doesn't exist");
        }
        hotelReservationTime.setHotelRoom(hotelRoom.get());
        try {
            reservationLock.lock();
            List<HotelRoom> rooms = listFreeRooms(hotelReservationTime.getStartReservation(), hotelReservationTime.getEndReservation());
            if (rooms.isEmpty() || rooms.stream().filter(r -> r.getId().equals(roomId)).findAny().isEmpty()) {
                throw new HotelApiException("time already used");
            }
            return hotelReservationTimeRepository.save(hotelReservationTime);
        } finally {
            reservationLock.unlock();
        }
    }

}
