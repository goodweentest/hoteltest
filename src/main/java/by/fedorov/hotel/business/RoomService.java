package by.fedorov.hotel.business;

import by.fedorov.hotel.domain.HotelRoom;

public interface RoomService {

    HotelRoom createHotelRoom(HotelRoom hotelRoom);

    HotelRoom updateHotelRoom(HotelRoom hotelRoom);

}
