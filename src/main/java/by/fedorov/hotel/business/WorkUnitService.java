package by.fedorov.hotel.business;

import by.fedorov.hotel.domain.HotelWorkUnit;

import java.util.List;

public interface WorkUnitService {

    HotelWorkUnit saveWorkUnit(HotelWorkUnit hotelWorkUnit);

    List<HotelWorkUnit> listWorkUnits();

}
