package by.fedorov.hotel.business;

import by.fedorov.hotel.domain.HotelReservation;
import by.fedorov.hotel.domain.HotelRoom;

import java.time.LocalDateTime;
import java.util.List;

public interface ReservationService {

    List<HotelRoom> listFreeRooms(LocalDateTime start, LocalDateTime end);

    <T extends HotelReservation> T reserve(Integer roomId, T hotelReservationTime);

}
