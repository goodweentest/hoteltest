package by.fedorov.hotel.business;

public class HotelApiException extends RuntimeException {

    public HotelApiException(String message) {
        super(message);
    }

}
