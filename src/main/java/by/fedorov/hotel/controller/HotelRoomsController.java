package by.fedorov.hotel.controller;

import by.fedorov.hotel.business.ReservationService;
import by.fedorov.hotel.business.RoomService;
import by.fedorov.hotel.configuration.SecurityConstants;
import by.fedorov.hotel.domain.HotelBooking;
import by.fedorov.hotel.domain.HotelCleaningReservation;
import by.fedorov.hotel.domain.HotelReservation;
import by.fedorov.hotel.domain.HotelRoom;
import lombok.AllArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.PermitAll;
import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping(ApiMapping.ROOMS)
public class HotelRoomsController {

    private RoomService roomService;
    private ReservationService reservationService;

    @PostMapping
    @Secured(SecurityConstants.ROLE_ADMIN)
    public HotelRoom create(@RequestBody HotelRoom hotelRoom) {
        return roomService.createHotelRoom(hotelRoom);
    }

    @PutMapping("{id}")
    @Secured(SecurityConstants.ROLE_ADMIN)
    public HotelRoom update(@RequestBody HotelRoom hotelRoom, @PathVariable("id") Integer id) {
        hotelRoom.setId(id);
        return roomService.updateHotelRoom(hotelRoom);
    }

    @GetMapping
    @PermitAll
    public List<HotelRoom> findRooms(
            @RequestParam("start") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm") LocalDateTime start,
            @RequestParam("end") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm") LocalDateTime end
            ) {
        return reservationService.listFreeRooms(start, end);
    }

    @Secured(SecurityConstants.ROLE_ADMIN)
    @PostMapping(ApiMapping.ROOMS_CLEANING_RESERVATIONS)
    public HotelReservation createCleaningReservation(
            @RequestBody HotelCleaningReservation roomCleaningReservation,
            @PathVariable("roomId") Integer roomId
    ) {
        return reservationService.reserve(roomId, roomCleaningReservation);
    }

    @Secured(SecurityConstants.ROLE_USER)
    @PostMapping(ApiMapping.ROOMS_USER_BOOKINGS)
    public HotelReservation createUserBooking(
            @RequestBody HotelBooking roomUserBooking,
            @PathVariable("roomId") Integer roomId
    ) {
        return reservationService.reserve(roomId, roomUserBooking);
    }

}
