package by.fedorov.hotel.controller;

import by.fedorov.hotel.business.WorkUnitService;
import by.fedorov.hotel.configuration.SecurityConstants;
import by.fedorov.hotel.domain.HotelWorkUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.PermitAll;
import java.util.List;

@RestController
@RequestMapping(ApiMapping.WORK_UNITS)
public class HotelWorkUnitsController {

    @Autowired
    private WorkUnitService workUnitService;

    @PostMapping
    @Secured(SecurityConstants.ROLE_ADMIN)
    public HotelWorkUnit createWorkUnit(@RequestBody HotelWorkUnit workUnit) {
        return workUnitService.saveWorkUnit(workUnit);
    }

    @GetMapping
    @PermitAll
    public List<HotelWorkUnit> getAll() {
        return workUnitService.listWorkUnits();
    }

}
