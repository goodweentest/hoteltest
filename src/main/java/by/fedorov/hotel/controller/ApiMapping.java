package by.fedorov.hotel.controller;

public interface ApiMapping {

    String API_BASE = "api/v1/";
    String WORK_UNITS = API_BASE + "workUnits";
    String ROOMS = API_BASE + "rooms";
    String ROOMS_CLEANING_RESERVATIONS = "{roomId}/cleaningReservations";
    String ROOMS_USER_BOOKINGS = "{roomId}/userBookings";

}
