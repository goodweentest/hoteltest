package by.fedorov.hotel.controller;

import by.fedorov.hotel.business.HotelApiException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class HotelApiControllerAdvice {

    @ExceptionHandler(HotelApiException.class)
    public ResponseEntity<String> handleHotelApiException(HotelApiException ex){
        return ResponseEntity.status(HttpStatus.CONFLICT).body(ex.getMessage());
    }

}
