package by.fedorov.hotel.repository;

import by.fedorov.hotel.domain.HotelReservation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HotelReservationTimeRepository extends JpaRepository<HotelReservation, Integer> {
}
