package by.fedorov.hotel.repository;

import by.fedorov.hotel.domain.HotelRoom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface HotelRoomRepository extends JpaRepository<HotelRoom, Integer> {

    @Query("select room from HotelRoom room where room.id not in ( " +
            "select room.id from HotelRoom room join room.reservations r where " +
            ":startTime <= r.startReservation and r.endReservation <= :endTime or " +
            ":startTime <= r.startReservation and :endTime > r.startReservation or " +
            ":startTime < r.endReservation and :endTime >= r.endReservation) group by room")
    List<HotelRoom> findFreeRooms(@Param("startTime") LocalDateTime start,
                                  @Param("endTime") LocalDateTime end);

}
