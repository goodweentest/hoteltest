package by.fedorov.hotel.repository;

import by.fedorov.hotel.domain.HotelWorkUnit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.DayOfWeek;

public interface HotelWorkUnitRepository extends JpaRepository<HotelWorkUnit, DayOfWeek> {
}
