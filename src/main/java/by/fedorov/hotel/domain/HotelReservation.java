package by.fedorov.hotel.domain;

import by.fedorov.hotel.utils.LocalDateTimeConverter;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class HotelReservation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "reservation_gen")
    @SequenceGenerator(name = "reservation_gen", sequenceName = "reservation_gen")
    private Integer id;

    @Convert(converter = LocalDateTimeConverter.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime startReservation;

    @Convert(converter = LocalDateTimeConverter.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime endReservation;

    @ManyToOne
    @JoinColumn(name = "hotel_room_id")
    private HotelRoom hotelRoom;

}
