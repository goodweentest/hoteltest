package by.fedorov.hotel.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class HotelRoom {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String title;

    private Integer places;

    private Long price;

    @OneToMany(mappedBy = "hotelRoom")
    @JsonIgnore
    private List<HotelReservation> reservations;

}
