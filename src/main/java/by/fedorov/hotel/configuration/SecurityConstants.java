package by.fedorov.hotel.configuration;

public interface SecurityConstants {

    String ROLE_PREFIX = "ROLE_";

    String ADMIN = "ADMIN";
    String USER = "USER";

    String ROLE_ADMIN = ROLE_PREFIX + ADMIN;
    String ROLE_USER = ROLE_PREFIX + USER;

}
